const GameEngine = require("@scrum_project/game-engine");

const readline = require("readline");

const linereader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const run = () => {
  const game = GameEngine.startGame();

  console.log("**********************");
  console.log("*  N E W    G A M E  *");
  console.log("**********************");
  game_loop(game);
};

const game_loop = game => {
  if (game.status == "GAMEOVER"){
    console.log("\n\n ################# GAMEOVER ###################\n\n")
    return;
  }else if(game.status == "WINNER"){
    console.log("\n\n !!!!!!!!!!!!!!! WINNER !!!!!!!!!!!!!!!!!!!")
    console.log("The word was " + game.word  +" \n\n")
    return;
  }else{
    printGameState(game);
    evaluateGuess(game);
  }
  
};

const printGameState = game => {
  console.log("Remaining Lives: " + game.lives);
  console.log("Word : ", game.display_word)
};

const evaluateGuess = game => {
  linereader.question("Guess a letter: ", letter => {
    const new_game_state = GameEngine.takeGuess(game,letter)
    game_loop(new_game_state);
  });
};



module.exports = {
  run
};
